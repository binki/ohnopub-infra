# SSL renewal

I just go to https://www.startssl.com/Certificates/ApplySSLCert and
follow the on-screen instructions. The key is stored at
`/etc/ssl/apache2/slatepermutate.org.key` and certificate at
`/etc/ssl/apache2/slatepermutate.org.crt`. There is also
`/etc/ssl/apache2/turbo.coffee.key` and friends…

    # cd /etc/ssl/apache2
    # umask 0077
    # openssl req -newkey rsa:2048 -keyout slatepermutate.org.key -nodes

Remember to add `www.slatepermutate.org` to the list of domains as
startssl suggests but to have without `www.` be the CN. After getting
cert downloaded from startssl, cat the certs to form a chain:

    $ cat 2_slatepermutate.org.crt 1_root_bundle.crt

Then paste them to hurin.ohnopub.net
`/etc/ssl/apache2/slatepermutate.org/crt`.

    # umask 0022
    # cat > slatepermutate.org.crt

Do same for `turbo.coffee`, `cdn.ohnopub.net`, `js.ohnopub.net`.
