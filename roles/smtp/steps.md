# smtp

For now I am just hurriedly setting up this system to get mail flowing
out of ohnopub.net through a relay we control semi-properly.

For now this is *outgoing only* and *only nbisgentoo64* mail. I.e.,
this is pre-scanned stuff. No accepting incoming connections. Using
nonstandard port and certificate auth.

This makes things simpler. However, I do still need to address the
following sorts of issues:

* domainkeys: daemon, DNS entries
* SPF: DNS entires

## Installing Gentoo

I have yet to have a generalized system setup guide. So this will be
the start of it and hopefully be refactored into something more
general eventually/become a libvirt script.

1. Get a linode ;-).

1. Name it “nina” after the daughter of the alchemist in Fullmetal
   Alchemist who was turned into a chimera.

1. Go to Dashboard -> Rebuild.

   1. Choose Gentoo 2014.12.

   1. Set swap image to largest available (512MB).

   1. Randomly generate a root password and save it in your usual
      store named root@nina.ohnopub.net_passwd.

1. Boot it.

1. Add nina to the DNS stuff and dnspush

1. Set rDNS in linode as nina.ohnopub.net (both IPv4 and IPv6)

1. lish to it, login as root, install screen, create ohnobinki, add ohnobinki to wheel, install nbisgentoo’s ssh key to ohnobinki

1. `ssh -t nina.ohnopub.net -- screen -x -R`

1. `su -l` up in a screen window or so

1. Remove gtk3 from USE and install emacs because I can’t live without it.

1. In the meantime, configure the hostname to nina to get rid of the annoying localhost prompt.

1. Install and configure metalog to run by default

1. Configure ssh to disallow password logins:
   1. set “ChallengeResponseAuthentication no” in `/etc/ssh/sshd_config`.

1. Configure SPF stuff in DNS to allow nina to send mail “a:nina.ohnopub.net”

1. Figure out how to install mail ^^.

   1. Add USE="cdb sasl"

   1. emerge postfix.

   1. Set up aliases root, ohnobinki -> ohnobinki@ohnopublishing.net /etc/mail/aliases, run newaliases

   1. emerge opendkim, set package.use -sasl, set net-dns/ldns[-ecdsa] for bindist. Use the emerge --config for it. Follow directions to set UMask to 002 and add postfix user to milter group. Follow instructions for configuring postfix and opendkim. Add opendkim to rc-update add opendkim default

   1. Configure postfix somehow

      1. Setup SSL key for it.

         smtp_tls_cert_file = /etc/postfix/nina.ohnopub.net.cert.pem
	 smtp_tls_key_file = /etc/postfix/nina.ohnopub.net.key.pem
	 smtpd_tls_cert_file = /etc/postfix/nina.ohnopub.net.cert.pem
	 smtpd_tls_key_file = /etc/postfix/nina.ohnopub.net.key.pem

         
         ( umask 0027; openssl req -newkey rsa:4096 -subj '/CN=nina.ohnopub.net/O=Oh! No! Publishing' -keyout nina.ohnopub.net.key.pem -out nina.ohnopub.net.req.pem -nodes; chown :postfix nina.ohnopub.net.key.pem; )

         cat the request and paste into startssl

         download the cert to your real computer because it does web ridiculousness and gives a zip file

         cat the 2_cert, 1_intermediate, root_cert into the file nina.ohnopub.net.cert.pem

     1. Configure random stuff

        uncomment “myorigin = $mydomain”
	set “mydestination =”
	set “mynetworks_style = host”
	set “relay_domains =” (this machine is set up only for *outgoing* mail for now, incoming will be later when we get more scanning and whatnot)

        For now these options will be set globally. But in the future, they will be enforced via maps or something?

	smtpd_tls_security_level = encrypt
	smtpd_tls_ask_ccert = yes
	smtpd_tls_fingerprint_digest = sha1

        smtpd_client_restrictions = permit_tls_clientcerts, check_relay_domains
	smtpd_relay_restrictions = permit_tls_clientcerts, check_relay_domains
	relay_clientcerts = inline:{7C:E0:7C:0E:ED:03:D5:A6:F5:56:CE:51:58:94:B9:1E:16:78:93:05=ohnopub.net.value.is.unused}
        get the cert_match value from running on nbisgentoo64: “openssl x509 -noout -fingerprint -sha1 -in /etc/ssl/postfixcert.pem”
