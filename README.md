Oh! No! Publishing Infra

I want to provide better services for ohnopub.net. This means many
things.

* Keeping software updated.
* Keeping better tabs on security.
* Using better security practices.
* Improving stability and reliability.

This means, among other things:

* Using real SSL certificates for everything.
* Having a testing environment.

To accomplish this, I want to:

* Have proper notification system for situations requiring human
  intervention.

   * Logging triggers for *critical* events which can be changed.

   * Notifications for recurrent operations such as renewing server
     certificates.

   * Detection of downtime/generation of notifications for that.

* Have a centralized status dashboard.

* Have scriptable and reproducible deployment

  * For a contained test environment

  * For actual production

  * Somehow dealing with porting/sharing data/state between instances

# How?

For now I’m going to try to start documenting and scripting things and
keep track of them in this repository. I am really not sure how to
structure this yet. For now, I am in dire need of setting up a proper
mail relay. I don’t have time to figure out how to make a reproducible
system for it, but I will try to record the commands I use and config
edits I make to configure it.

## Technologies

Puppet might be useful for managing most of the system build
process. Or perhaps Gentoo catalyst. I could research how they do
things these days.

My scripts will be against libvirt if I can figure that out. It would
be cool if libvirt had backends for AWS/linode/prgmr. Maybe there
exists something more meta than libvirt which uses libvirt as a
backend and supports backending to paid providers?

When looking at libvirt’s options, I noticed a technology I had
forgotten about which it supports as a backend: User Mode Linux. Since
VMs are expensive, I could probably use UML inside of a VM to get
relatively clean per “role” environments and have each “role” be its
own environment without needing a “real” VM per role. Though I will
likely need to figure out how to make my systems as light as possible
if going this route ;-).

Oh, linode seems to have a concept of StackScripts. That sounds like
something like possibly a libvirt/AWS/hosting-service agnostic name or
something that might be the name of something which does that.

## Future architecture

It would be nice to have the following sorts of services:

* id.ohnopub.net: kerberos, letsauth (err, Portier via OpenID Connect with Email Discovery), simple web auth/password
   change.

* mail.ohnopub.net: hosted user-specific mail stuff such as webmail,
   user-authenticated outgoing SMTP, IMAP (so some big chunk of data
   that will need to be managed somehow). The postfix here would be
   very simple and only accept user-auth or from smtp.ohnopub.net and
   have relayhost smtp.ohnopub.net.

* smtp.ohnopub.net: relay, scanning, entry point of incoming mail,
   exit point for outgoing mail—to be separated from the user hosted
   stuff. Domain forwards stuff such as for protofusion.org would be
   configured to point to this.

Then the arbitrary first-class branded services:

* irc.ohnopub.net
* slatepermutate.org
* turbo.coffee

## Problems to be solved

1. Where to store undisclosable information. E.g., root
   passwords. Currently using my usual password storage mechanism.

2. An important event that needs notification: GPG key expiry

## Dashboard things

* smtp

   * hourly checks that email can successfully be sent to GMail, Yahoo!, Comcast, Outlook.com, sbcglobal.net (currently broken?)

   * count of deferred messages

* domains

   * 10-minute DNS lookup verifications for various domains

   * daily WHOIS expiry checks for various domains (make sure that the
      tolerance time for auto-renew causes auto-renew to trigger
      before this dashboard alarm goes off)

* SSL

  * Use s_client to check for expiries and show countdown and set alarms

* ML

  * number of pending approvals >0 is yellow

  * number of any pending approvals *from ML members*. >0 is red (most
    likely these messages should go through or have followup directly
    with sender)

  * number of people with delivery disabled. >0 is red

## Hot issues

hurin is listed as MX but I’ve blocked his port 25 because I think he is introducing spam because he isn’t probably filtered. Step 1 of roles/etc. is setting up a proper way for incoming mail to be handled on a system that does all the filterings, etc.

## mail

how to manage held messages in queue:

    while true; do qid=$(mailq | sed -n -e '2s/!.*//p'); [ "${qid}" ] || break; postcat /var/spool/postfix/hold/"${qid}" | less; echo -n "(d)elete, e(x)it> "; read c; [ "${c}" == "d" ] && postsuper -d "${qid}"; [ "${c}" == "x" ] && break; done
